## redisdemo
### redis的学习网站：
1.  redis.cn
2.  redis.io
3.  db-engines.com

###  讲课用的图片在IMAGE目录中
```
1.  01Redis前无古人后无来者
2.  02REDIS集群知识点
3.  pos格式的文件可以导入到 https://processon.com/  或者其他的脑图软件
```

###  API代码的学习：
```
1.  redis.io 的client 中有JAVA语言的客户端：jedis、lettuce等可以分别访问他们的github学习
2.  另外是基于spring的使用，主动通过spring.io官网学习spring.data.redis
3.  spring.io中:   https://spring.io/projects/spring-data-redis
```

### gcc升级到9.3
yum -y install centos-release-scl
yum -y install devtoolset-9-gcc devtoolset-9-gcc-c++ devtoolset-9-binutils
scl enable devtoolset-9 bash
需要注意的是scl命令启用只是临时的，退出shell或重启就会恢复原系统gcc版本。
如果要长期使用gcc 9.3的话：
echo "source /opt/rh/devtoolset-9/enable" >>/etc/profile

### Linux安装Redis 6.0.6 ./install_server.sh报错

vi ./install_server.sh
注释下面的代码

#bail if this system is managed by systemd
#_pid_1_exe="$(readlink -f /proc/1/exe)"
#if [ "${_pid_1_exe##*/}" = systemd ]
#then
#       echo "This systems seems to use systemd."
#       echo "Please take a look at the provided example service unit files in this directory, and adapt and install them. Sorry!"
#       exit 1
#fi
然后重新运行 ./install_server.sh即可。

### redis 启动命令
service redis_6379 start/stop/status/restart

